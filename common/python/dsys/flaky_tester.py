import threading
import time
import logging

import docker
from docker.models import networks as dm_networks

from . import docker_util
from . import interfaces


log = logging.getLogger('flaky_tester')


def run_flaky(cmd, docker_client, docker_network, build_root, course_root, data_producer, taint_manager, log_root):
    """

    :type cmd: typing.List[str]
    :type docker_client: docker.DockerClient
    :type docker_network: dm_networks.Network
    :type build_root: str
    :type course_root: str
    :type data_producer: interfaces.IProducer
    :type taint_manager: docker_util.TaintManager
    :type log_root: str

    """
    # Creating bunch of user containers
    for i in range(10):
        user_container = docker_util.UserContainerWrapper(
            cmd,
            docker_client,
            docker_network,
            build_root,
            course_root,
            log_root,
        )
        user_container.create()
        taint_manager.register_container(user_container)

    producer_thread = threading.Thread(target=data_producer)
    taint_beat = 0
    last_ts = time.time()
    try:
        producer_thread.start()
        for c in taint_manager.containers:
            log.warning('Starting container: {}'.format(c.container_name))
            c.start()

        wait_it = 0
        while data_producer.termination.is_set() is False and data_producer.completed_ev.is_set() is False:
            wait_it += 1
            log.warning('Wait iteration: {}'.format(wait_it))
            for c in taint_manager.containers:
                if c.ready():
                    msg = 'Container {} terminated abnormally: {}'.format(c.container_name, c.container.status)
                    log.error(msg)
                    c.wait(5)
                    data_producer.termination.set()
                    raise AssertionError(msg)
            time.sleep(1)
            # tick 1s
            # session 4s (min 2s)
            taint_beat += time.time() - last_ts
            last_ts = time.time()
            if taint_beat > 3:
                taint_beat = 0
                taint_manager.reschedule_taints()

        if data_producer.completed_ev.is_set():
            log.warning('Completed event set!')
            taint_manager.remove_all_taints()
            time.sleep(30)
    except BaseException as err:
        log.exception('Test failed: {}'.format(err))
        data_producer.termination.set()
        raise
    finally:
        try:
            producer_thread.join(timeout=5)
            for c in taint_manager.containers:  # type: docker_util.UserContainerWrapper
                exit_code = c.wait(timeout=10)
                if data_producer.termination.is_set() is False:
                    assert exit_code == 0
                c.terminate()
        except BaseException as err:
            log.exception('Base exception at cleanup: {}'.format(err))
            if data_producer.termination.is_set() is False:
                raise

