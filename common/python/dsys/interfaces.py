import threading


class IProducer(object):
    def __call__(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def completed_ev(self):
        """

        :rtype: threading.Event
        """
        raise NotImplementedError

    @property
    def termination(self):
        """

        :rtype: threading.Event
        """
        raise NotImplementedError
