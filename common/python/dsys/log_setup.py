import logging


def init_logging():
    # disable requests module logging
    logging.getLogger('requests').setLevel(logging.DEBUG)
    logging.getLogger('requests.packages.urllib3.connectionpool').setLevel(logging.ERROR)
    logging.getLogger('kazoo').setLevel(logging.INFO)
