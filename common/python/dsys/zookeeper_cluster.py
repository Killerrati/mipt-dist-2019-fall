from docker import types as docker_types
from docker.models import containers as dm_containers
from docker.models import networks as dm_networks
import docker
import docker.errors
import docker.models
import os
from . import docker_util
from . import netfilter

import logging

log = logging.getLogger('ZooKeeperClusterManager')

LOG4_JAVA_PROPERTIES = """
log4j.rootLogger=${zookeeper.root.logger}

log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender
log4j.appender.CONSOLE.Threshold=INFO
log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout
log4j.appender.CONSOLE.layout.ConversionPattern=%d{ISO8601} - %-5p [%t:%C{1}@%L] - %m%n

log4j.appender.ROLLINGFILE=org.apache.log4j.RollingFileAppender
log4j.appender.ROLLINGFILE.Threshold=DEBUG
log4j.appender.ROLLINGFILE.File=${zookeeper.log.dir}/${zookeeper.log.zkLogName}

# Max log file size of 10MB
log4j.appender.ROLLINGFILE.MaxFileSize=100MB
# uncomment the next line to limit number of backup files
log4j.appender.ROLLINGFILE.MaxBackupIndex=10

log4j.appender.ROLLINGFILE.layout=org.apache.log4j.PatternLayout
log4j.appender.ROLLINGFILE.layout.ConversionPattern=%d{ISO8601} - %-5p [%t:%C{1}@%L] - %m%n


log4j.appender.TRACEFILE=org.apache.log4j.FileAppender
log4j.appender.TRACEFILE.Threshold=TRACE
log4j.appender.TRACEFILE.File=${zookeeper.log.dir}/${zookeeper.log.zkTraceLogName}

log4j.appender.TRACEFILE.layout=org.apache.log4j.PatternLayout
log4j.appender.TRACEFILE.layout.ConversionPattern=%d{ISO8601} - %-5p [%t:%C{1}@%L][%x] - %m%n
"""

ZOOKEEPER_CONFIG_TEMPLATE = """
tickTime=1000
initLimit=10
syncLimit=5
dataDir=/zookeeper_data/data
clientPort=2181
leaderServes=yes
4lw.commands.whitelist=*

autopurge.purgeInterval=1
autopurge.snapRetainCount=10

server.1=zk-master-1-{0}:2888:3888
server.2=zk-master-2-{0}:2888:3888
server.3=zk-master-3-{0}:2888:3888
"""


class ZookeeperCluster(object):
    def __init__(self, docker_network, docker_client, zookeeper_dist, tmpdir):
        """

        :type docker_network: dm_networks.Network
        :type docker_client: docker.DockerClient
        :type zookeeper_dist: str
        :type tmpdir: py.path.local
        """
        self.docker_network = docker_network
        self.docker_client = docker_client
        self.zookeeper_dist = zookeeper_dist
        self.tmpdir = tmpdir
        self.containers = []

    def register_at_netfilter(self, net_filter):
        """

        :type net_filter: netfilter.NetFilter

        """
        for zk_container in self.containers:
            address = docker_util.get_container_address(zk_container, self.docker_network)
            net_filter.set_control_plane_host(address)
            net_filter.connect_host(address)

    @property
    def connection_string(self):
        """

        :rtype: str
        """
        zookeeper_connection_string = ','.join(
            docker_util.get_container_address(x, self.docker_network) + ':2181' for x in self.containers
        )
        return zookeeper_connection_string

    def __enter__(self):
        class_root = os.path.join(self.zookeeper_dist, 'lib')
        classpath = [
            os.path.join('/zookeeper/lib', x)
            for x in os.listdir(class_root)
            if x.endswith('.jar')
        ]
        zookeeper_root = self.tmpdir.join('zookeeper_root')

        zookeeper_image = 'openjdk:13'
        try:
            self.docker_client.images.get(zookeeper_image)
        except docker.errors.ImageNotFound:
            self.docker_client.images.pull(zookeeper_image)

        cmd = [
            '/usr/java/openjdk-13/bin/java',
            '-cp', ':'.join(classpath),
            '-Xmx512M',
            '-Djava.net.preferIPv6Addresses=true',
            '-Dzookeeper.log.dir=/zookeeper_data/logs',
            '-Dzookeeper.root.logger=INFO,ROLLINGFILE',
            '-DdataDir=/zookeeper_data/data',
            '-Dlog4j.debug=True',
            '-Dzookeeper.log.zkTraceLogName=zookeeper-trace.log',
            '-Dzookeeper.log.zkLogName=zookeeper-main.log',
            '-Dlog4j.configuration=file:///zookeeper_data/etc/log4j.properties',
            'org.apache.zookeeper.server.quorum.QuorumPeerMain',
            '/zookeeper_data/etc/zoo.cfg',
        ]
        for i in range(1, 4):
            master_id = 'zk-master-{}-{}'.format(i, os.environ.get('EXECUTOR_NUMBER', '0'))
            instance_root = zookeeper_root.join(master_id)
            instance_etc = instance_root.join('etc')
            instance_data = instance_root.join('data')
            instance_logs = instance_root.join('logs')

            instance_etc.ensure(dir=True)
            instance_data.ensure(dir=True)
            instance_logs.ensure(dir=True)

            instance_etc.join('log4j.properties').write(LOG4_JAVA_PROPERTIES)
            instance_etc.join('zoo.cfg').write(ZOOKEEPER_CONFIG_TEMPLATE.format(os.environ.get('EXECUTOR_NUMBER', '0')))
            instance_data.join('myid').write(str(i))

            container = self.docker_client.containers.create(
                image=zookeeper_image,
                network=self.docker_network.name,
                user=os.getuid(),
                # group=os.getgid(),
                mounts=[
                    docker_types.Mount(
                        source=self.zookeeper_dist,
                        target='/zookeeper',
                        read_only=True,
                        type='bind',
                    ),
                    docker_types.Mount(
                        source=str(instance_root),
                        target='/zookeeper_data',
                        read_only=False,
                        type='bind',
                    )
                ],
                name=master_id,
                hostname=master_id,
                command=cmd,
                # command=['/bin/sleep', '34000']
            )  # type: dm_containers.Container
            self.containers.append(container)

        for container in self.containers:
            container.start()
            container.reload()
            log.warning('Started zookeeper peer. (ContainerId: {}, Name: {}, Address: {})'.format(
                container.short_id,
                container.name,
                docker_util.get_container_address(container, self.docker_network),
            ))
        docker_util.print_network_containers(self.docker_network)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        for container in self.containers:
            container.kill()
            container.wait()
            for line in container.logs().splitlines():
                log.warning('{} >> {}'.format(container.name, line))

        for container in self.containers:
            log.warning('Removing container: {}'.format(container.short_id))
            container.remove(force=True)
