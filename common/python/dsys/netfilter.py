import random
import subprocess
import time
import typing

import ipaddress
import logging

log = logging.getLogger('NetFilter')


class NetFilter(object):
    IPT_WAIT = '20'

    def __init__(self, device, network, gateway):
        self.device = device
        self.network = ipaddress.ip_network(unicode(network))
        self.gw = ipaddress.ip_address(gateway)
        self.online_hosts = dict(
            (x, False) for x in self.network.hosts() if x != self.gw
        )
        self.partial_connects = dict()
        self.chain_name = 'DSYS_CHAIN-' + device
        self.control_plane_hosts = []  # type: typing.List[ipaddress.IPv4Address]
        self.rng = random.Random(54579247)
        self._launch_id = 1

    def _log_launch(self, cmd):
        self._launch_id += 1
        lid = self._launch_id
        ts = time.time()
        rc = 4
        while rc == 4:
            log.info('Spawn [{}] {}'.format(lid, cmd))
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            rc = p.wait()
            te = time.time()
            log.info('Launch finished. LaunchId: {}, Time: {}s, ExitCode: {}'.format(lid, te - ts, rc))
            stdout = p.stdout.read()
            stderr = p.stderr.read()
            if stdout:
                for item in stdout.splitlines():
                    log.info('STDOUT: ' + item)
            if stderr:
                for item in stderr.splitlines():
                    log.info('STDERR: ' + item)

            if rc == 4:
                time.sleep(0.1)
        return rc

    def set_control_plane_host(self, host):
        self.control_plane_hosts.append(ipaddress.ip_address(host))

    def connect_host(self, host):
        host = ipaddress.ip_address(host)
        assert self.online_hosts[host] is False
        assert host not in self.partial_connects
        self.online_hosts[host] = True
        cmd = ['iptables', '-w', self.IPT_WAIT, '-I', self.chain_name, '-s', str(host), '-j', 'ACCEPT']
        rc = self._log_launch(cmd)
        assert rc == 0

    def disconnect_host(self, host):
        host = ipaddress.ip_address(host)
        assert self.online_hosts[host] is True
        assert host not in self.partial_connects
        self.online_hosts[host] = False
        cmd = ['iptables', '-w', self.IPT_WAIT, '-D', self.chain_name, '-s', str(host), '-j', 'ACCEPT']
        rc = self._log_launch(cmd)
        assert rc == 0

    def set_partial_connect(self, host):
        host = ipaddress.ip_address(host)
        assert self.online_hosts[host] is True
        assert host not in self.partial_connects
        allowed_destination = self.rng.choice(self.control_plane_hosts)
        self.partial_connects[host] = allowed_destination
        cmd = ['iptables', '-w', self.IPT_WAIT, '-I', self.chain_name, '-s', str(host), '-d', str(allowed_destination),
               '-j', 'ACCEPT']
        rc = self._log_launch(cmd)
        assert rc == 0

    def remove_partial_connect(self, host):
        host = ipaddress.ip_address(host)
        assert self.online_hosts[host] is True
        assert host in self.partial_connects
        allowed_destination = self.partial_connects[host]
        del self.partial_connects[host]
        cmd = ['iptables', '-w', self.IPT_WAIT, '-D', self.chain_name, '-s', str(host), '-d', str(allowed_destination),
               '-j', 'ACCEPT']
        rc = self._log_launch(cmd)
        assert rc == 0

    def cleanup(self):
        commands = [
            ['iptables', '-w', self.IPT_WAIT, '-F', self.chain_name],
            ['iptables', '-w', self.IPT_WAIT, '-D', 'FORWARD', '-i', self.device, '-j', self.chain_name],
            ['iptables', '-w', self.IPT_WAIT, '-X', self.chain_name],
        ]
        for cmd in commands:
            rc = self._log_launch(cmd)
            if rc != 0:
                log.error('Command failed with code {}'.format(rc))

    def _init(self):
        commands = [
            ['iptables', '-w', self.IPT_WAIT, '-N', self.chain_name],
            ['iptables', '-w', self.IPT_WAIT, '-A', self.chain_name, '-d', str(self.network.broadcast_address), '-j',
             'ACCEPT'],
            ['iptables', '-w', self.IPT_WAIT, '-A', self.chain_name, '-d', str(self.network.network_address), '-j',
             'ACCEPT'],
            ['iptables', '-w', self.IPT_WAIT, '-A', self.chain_name, '-d', str(self.gw), '-j', 'ACCEPT'],
            ['iptables', '-w', self.IPT_WAIT, '-A', self.chain_name, '-s', str(self.network.broadcast_address), '-j',
             'ACCEPT'],
            ['iptables', '-w', self.IPT_WAIT, '-A', self.chain_name, '-s', str(self.network.network_address), '-j',
             'ACCEPT'],
            ['iptables', '-w', self.IPT_WAIT, '-A', self.chain_name, '-s', str(self.gw), '-j', 'ACCEPT'],
            ['iptables', '-w', self.IPT_WAIT, '-A', self.chain_name, '-j', 'DROP'],
            ['iptables', '-w', self.IPT_WAIT, '-I', 'FORWARD', '-i', self.device, '-j', self.chain_name],
        ]
        for cmd in commands:
            rc = self._log_launch(cmd)
            assert rc == 0

    def __enter__(self):
        self.cleanup()
        self._init()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cleanup()
