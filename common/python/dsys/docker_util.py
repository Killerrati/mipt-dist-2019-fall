import docker
import logging
import os
import random
import signal
import string

import typing
import docker.errors
from docker import types as docker_types
from docker.models import containers as dm_containers
from docker.models import networks as dm_networks

from . import netfilter

log = logging.getLogger('docker_utils')


def get_network_subnet(network):
    """

    :type network: dm_networks.Network
    """
    ipam = network.attrs['IPAM']
    subnet = ipam["Config"][0]["Subnet"]
    gateway = ipam["Config"][0]["Gateway"]
    size = len('f1687e83033e')
    device = 'br-' + network.attrs["Id"][:size]
    return device, subnet, gateway


def print_network_containers(docker_network):
    docker_network.reload()
    msg = []
    for c in docker_network.containers:  # type: dm_containers.Container
        net_settings = c.attrs['NetworkSettings']['Networks'][docker_network.name]
        msg.append('(Name: {0.name}, Id: {0.short_id}, Address: {1})'.format(c, net_settings['IPAddress']))
    log.warning('Network: {}, Containers: ({})'.format(
        docker_network.name,
        ', '.join(sorted(msg))
    ))


def get_container_address(container, network):
    """

    :type container: dm_containers.Container
    :type network: dm_networks.Network
    :return:
    """
    net_settings = container.attrs['NetworkSettings']['Networks'][network.name]
    return net_settings['IPAddress']


class ContainerTaints(object):
    NONE = 'NONE'
    OFFLINE = 'OFFLINE'
    PARTIAL = 'PARTIAL'
    PAUSED = 'PAUSED'
    TERMINATED = 'TERMINATED'

    ALL = [NONE, OFFLINE, PARTIAL, PAUSED, TERMINATED]


class UserContainerWrapper(object):
    user_image = 'mistyfungus/fivt_dsys:latest'

    def __init__(self, cmd, docker_client, docker_network, build_root, course_root, log_root):
        """

        :type cmd: typing.List[str]
        :type docker_client: docker.DockerClient
        :type docker_network: dm_networks.Network
        :type build_root: str
        :type course_root: str
        :type log_root: str
        """
        self.cmd = cmd
        self.docker_client = docker_client
        self.docker_network = docker_network
        self.build_root = build_root
        self.course_root = course_root
        self.log_root = log_root
        self.container_name = 'user_container-' + ''.join(random.sample(string.ascii_lowercase, 5))
        self.container = None  # type: typing.Optional[dm_containers.Container]

    def _ensure_image(self):
        try:
            self.docker_client.images.get(self.user_image)
        except docker.errors.ImageNotFound:
            log.warning('Pulling user image {} ...'.format(self.user_image))
            self.docker_client.images.pull(self.user_image)

    def create(self):
        self._ensure_image()

        self.container = self.docker_client.containers.create(
            image=self.user_image,
            network=self.docker_network.name,
            user=os.getuid(),
            mounts=[
                docker_types.Mount(
                    source=self.build_root,
                    target=self.build_root,
                    read_only=True,
                    type='bind',
                ),
                docker_types.Mount(
                    source=self.course_root,
                    target=self.course_root,
                    read_only=True,
                    type='bind',
                ),

            ],
            name=self.container_name,
            hostname=self.container_name,
            command=self.cmd,
        )  # type: dm_containers.Container

    def start(self):
        self.container.start()
        self.container.reload()

    def pause(self):
        self.container.pause()
        self.container.reload()

    def resume(self):
        self.container.unpause()
        self.container.reload()

    @property
    def address(self):
        assert self.container
        return get_container_address(self.container, self.docker_network)

    def ready(self):
        self.container.reload()
        return self.container.status not in ['running', 'paused']

    def wait(self, timeout):
        """

        :type timeout: int
        :rtype: int
        """
        try:
            exit_code = self.container.wait(timeout=timeout)['StatusCode']
        except Exception as err:
            log.error('Timed out waiting for container: Container: {}, Error: {}'.format(self.container_name, err))
            exit_code = -9
            self.container.kill(signal=signal.SIGKILL)

        log.info('User container exited with code: {}'.format(exit_code))
        stdout_path = os.path.join(self.log_root, self.container_name + '.stdout')
        stderr_path = os.path.join(self.log_root, self.container_name + '.stderr')
        log.warning('Saving stdout to  {}'.format(stdout_path))
        logs = self.container.logs(stdout=True, stderr=False)
        with open(stdout_path, 'w') as f:
            f.write(logs)
        log.warning('Saving stderr to  {}'.format(stderr_path))
        logs = self.container.logs(stdout=False, stderr=True)
        with open(stderr_path, 'w') as f:
            f.write(logs)

        return exit_code

    def terminate(self):
        self.container.remove(force=True)


class TaintManager(object):
    def __init__(self, net_filter):
        """

        :type net_filter: netfilter.NetFilter
        """
        self.net_filter = net_filter
        self.containers = []  # type: typing.List[UserContainerWrapper]
        self.taints = []
        self.rng = random.Random(2225637)

    def register_container(self, container):
        """

        :type container: docker_util.UserContainerWrapper
        """
        assert container.container_name not in [x.container_name for x in self.containers]
        self.containers.append(container)
        self.taints.append(ContainerTaints.OFFLINE)  # Default offline

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        for container in self.containers:
            container.terminate()

    def update_taint(self, container_idx, taint):
        """

        :type container_idx: int
        :type taint: str
        """
        old_taint = self.taints[container_idx]
        container = self.containers[container_idx]
        if old_taint == taint:
            log.warning('Keep taint. ContainerName: {}, TaintId: {}'.format(container.container_name, old_taint))
            return

        log.warning('Remove taint. ContainerName: {}, TaintId: {}'.format(container.container_name, old_taint))
        # Remove old taint
        self.taints[container_idx] = ContainerTaints.NONE
        if old_taint == ContainerTaints.NONE:
            pass
        elif old_taint == ContainerTaints.OFFLINE:
            self.net_filter.connect_host(container.address)
        elif old_taint == ContainerTaints.PARTIAL:
            self.net_filter.remove_partial_connect(container.address)
        elif old_taint == ContainerTaints.PAUSED:
            container.resume()

        log.warning('Set taint. ContainerName: {}, TaintId: {}'.format(container.container_name, taint))
        self.taints[container_idx] = taint
        if taint == ContainerTaints.NONE:
            pass
        elif taint == ContainerTaints.OFFLINE:
            self.net_filter.disconnect_host(container.address)
        elif taint == ContainerTaints.PARTIAL:
            self.net_filter.set_partial_connect(container.address)
        elif taint == ContainerTaints.PAUSED:
            container.pause()

    def reschedule_taints(self):
        counts = {
            ContainerTaints.NONE: 2,
            ContainerTaints.PAUSED: 3,
            ContainerTaints.PARTIAL: 2,
            ContainerTaints.OFFLINE: 3,
        }

        taints = [x for x, y in counts.iteritems() for _ in range(y)]
        self.rng.shuffle(taints)
        taints = taints[:len(self.containers)]

        for container_idx, taint in enumerate(taints):
            self.update_taint(container_idx, taint)

    def remove_all_taints(self):
        log.warning('Removing all taints')
        for i in range(len(self.containers)):
            self.update_taint(i, ContainerTaints.NONE)
