import threading
import time
import logging

import docker
from docker.models import networks as dm_networks

from . import docker_util
from . import interfaces


log = logging.getLogger('steady_tester')


def run_steady(cmd, docker_client, docker_network, build_root, course_root, data_producer, log_root):
    """

    :type cmd: typing.List[str]
    :type docker_client: docker.DockerClient
    :type docker_network: dm_networks.Network
    :type build_root: str
    :type course_root: str
    :type data_producer: interfaces.IProducer
    :type log_root: str
    """

    # Creating bunch of user containers
    user_containers = []
    for i in range(10):
        user_container = docker_util.UserContainerWrapper(
            cmd,
            docker_client,
            docker_network,
            build_root,
            course_root,
            log_root,
        )
        user_container.create()
        user_containers.append(user_container)

    processor_thread = threading.Thread(target=data_producer)
    try:
        processor_thread.start()
        for c in user_containers:
            log.warning('Starting container: {}'.format(c.container_name))
            c.start()

        wait_it = 0
        while data_producer.termination.is_set() is False and data_producer.completed_ev.is_set() is False:
            wait_it += 1
            log.warning('Wait iteration: {}'.format(wait_it))
            for c in user_containers:
                if c.ready():
                    msg = 'Container {} terminated abnormally: {}'.format(c.container_name, c.container.status)
                    log.error(msg)
                    data_producer.termination.set()
                    raise AssertionError(msg)
            time.sleep(3)

        if data_producer.completed_ev.is_set():
            # Remove taints
            time.sleep(30)

    finally:
        processor_thread.join(timeout=5)
        for c in user_containers:
            exit_code = c.wait(timeout=5)
            if data_producer.termination.is_set() is False:
                assert exit_code == 0
            c.terminate()


