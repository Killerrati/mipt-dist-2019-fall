#pragma once

namespace dsys {
/* zookeeper state constants */
#define EXPIRED_SESSION_STATE_DEF -112
#define AUTH_FAILED_STATE_DEF -113
#define CONNECTING_STATE_DEF 1
#define ASSOCIATING_STATE_DEF 2
#define CONNECTED_STATE_DEF 3
#define READONLY_STATE_DEF 5
#define NOTCONNECTED_STATE_DEF 999

static const char* state2String(int state){
  switch(state){
    case 0:
      return "ZOO_CLOSED_STATE";
    case CONNECTING_STATE_DEF:
      return "ZOO_CONNECTING_STATE";
    case ASSOCIATING_STATE_DEF:
      return "ZOO_ASSOCIATING_STATE";
    case CONNECTED_STATE_DEF:
      return "ZOO_CONNECTED_STATE";
    case READONLY_STATE_DEF:
      return "ZOO_READONLY_STATE";
    case EXPIRED_SESSION_STATE_DEF:
      return "ZOO_EXPIRED_SESSION_STATE";
    case AUTH_FAILED_STATE_DEF:
      return "ZOO_AUTH_FAILED_STATE";
    default:
      return "INVALID_STATE";
  }
}



/* zookeeper event type constants */
#define CREATED_EVENT_DEF 1
#define DELETED_EVENT_DEF 2
#define CHANGED_EVENT_DEF 3
#define CHILD_EVENT_DEF 4
#define SESSION_EVENT_DEF -1
#define NOTWATCHING_EVENT_DEF -2

static const char* watcherEvent2String(int ev){
  switch(ev){
    case 0:
      return "ZOO_ERROR_EVENT";
    case CREATED_EVENT_DEF:
      return "ZOO_CREATED_EVENT";
    case DELETED_EVENT_DEF:
      return "ZOO_DELETED_EVENT";
    case CHANGED_EVENT_DEF:
      return "ZOO_CHANGED_EVENT";
    case CHILD_EVENT_DEF:
      return "ZOO_CHILD_EVENT";
    case SESSION_EVENT_DEF:
      return "ZOO_SESSION_EVENT";
    case NOTWATCHING_EVENT_DEF:
      return "ZOO_NOTWATCHING_EVENT";
    default:
      return "INVALID_EVENT";
  }
}
}  // dsys