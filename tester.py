#!/usr/bin/env python2.7
import argparse
import errno
import os
import subprocess
import sys

BUILD_TYPES = (
    'debug',
    'release',
    'asan',
    'tsan',
)


def run_build(task, build, destination):
    build_root = os.path.join(destination, '{}_{}'.format(task, build))
    build_target = '_'.join(task.split('-')[1:])
    try:
        os.makedirs(build_root)
    except OSError as err:
        if err.errno != errno.EEXIST:
            raise
    real_build = 'DEBUG' if build != 'release' else 'release'
    root = os.path.dirname(os.path.abspath(__file__))
    cmd = ['cmake', root, '-DCMAKE_BUILD_TYPE={}'.format(real_build), '-DCMAKE_C_COMPILER=/usr/bin/clang-6.0', '-DCMAKE_CXX_COMPILER=/usr/bin/clang++-6.0']
    if build == 'asan':
        cmd.append('-DASAN=1')
    if build == 'tsan':
        cmd.append('-DTSAN=1')

    p = subprocess.Popen(
        args=cmd,
        cwd=build_root,
    )
    p.wait()
    if p.returncode != 0:
        return p.returncode

    p = subprocess.Popen(
        args=['make', build_target],
        cwd=build_root,
    )
    p.wait()
    return p.returncode


def run_test(task, build, destination):
    build_root = os.path.join(destination, '{}_{}'.format(task, build))
    test_target = 'test_' + '_'.join(task.split('-')[1:])
    p = subprocess.Popen(
        args=['make', test_target],
        cwd=build_root,
    )
    p.wait()
    return p.returncode


BRANCH_MAP = {
    'zktask1': '0-hello-world',
    'zktask2': '1-sequential-consumer',
    'zktask3': '2-parallel-consumer',
}


def main():
    root = os.path.dirname(os.path.abspath(__file__))
    tasks_dir = os.path.join(root, 'tasks')
    tasks = os.listdir(tasks_dir)
    tasks = [x for x in tasks if os.path.isdir(os.path.join(tasks_dir, x))]
    build_root = os.path.join(root, 'build')

    parser = argparse.ArgumentParser()
    parser.add_argument("task", help="Task to test: {}".format(tasks))
    parser.add_argument("--build", help='Build type: {}, Default="all"'.format(BUILD_TYPES), default='all')
    parser.add_argument("--destination", help='Build root, Default="{}"'.format(build_root), default=build_root)
    parser.add_argument("--no-build", help='Skip build', action='store_true')
    parser.add_argument("--no-test", help='Skip test', action='store_true')
    parser.add_argument("--branch-check", help='Check branch name', action='store_true')
    args = parser.parse_args()

    if args.branch_check:
        if args.task not in BRANCH_MAP.keys():
            print >> sys.stderr, "Invalid branch name: {}. Supported: {}".format(args.task, BRANCH_MAP.keys())
            sys.exit(1)
        sys.exit(0)

    if args.task in tasks:
        guessed_task = args.task
    else:
        guessed_task = BRANCH_MAP.get(args.task, None)

    if guessed_task is None:
        print >> sys.stderr,  'Unknown task: {}'.format(args.task)
        sys.exit(1)
    assert guessed_task in tasks
    assert args.build in list(BUILD_TYPES) + ['all'], 'Unknown build type: {}'.format(args.build)

    builds = [args.build] if args.build != 'all' else BUILD_TYPES

    for build in builds:
        if args.no_build is False:
            rv = run_build(guessed_task, build, args.destination)
            if rv != 0:
                print >> sys.stderr, 'ERROR: make [{}] failed with code {}'.format(build, rv)
                sys.exit(rv)

    for build in builds:
        if args.no_test is False:
            rv = run_test(guessed_task, build, args.destination)
            if rv != 0:
                print >> sys.stderr, 'ERROR: test [{}] failed with code {}'.format(build, rv)
                sys.exit(rv)


if __name__ == '__main__':
    main()
