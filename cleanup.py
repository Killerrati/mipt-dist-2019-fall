#!/usr/bin/env python
import sys
import os

import docker
from docker.models import containers as dm_containers
from docker.models import networks as dm_networks

root = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(root, 'common', 'python'))

from dsys import docker_fixtures
from dsys import netfilter
from dsys import docker_util


def main():
    net_name = docker_fixtures.TEST_NETWORK_NAME + '-e' + os.environ.get('EXECUTOR_NUMBER', '0')
    client = docker.from_env()

    try:
        network = client.networks.get(net_name)  # type: dm_networks.Network
        print "Goot network", network.name, network.short_id
    except Exception as err:
        print str(err)
        return
    # cleanup containers

    for guest in network.containers:  # type: dm_containers.Container
        print 'Remove ', guest.name, guest.short_id
        try:
            guest.remove(force=True)
        except Exception as err:
            print str(err)

    try:
        nf = netfilter.NetFilter(*docker_util.get_network_subnet(network))
        nf.cleanup()
    except Exception as err:
        print set(err)

    network.remove()


if __name__ == '__main__':
    main()
