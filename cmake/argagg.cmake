set(ROOT ${COURSE_ROOT}/contrib/argagg)
add_library(arcagg INTERFACE)
target_include_directories(arcagg INTERFACE
        $<BUILD_INTERFACE:${ROOT}/include>
        $<INSTALL_INTERFACE:include>)