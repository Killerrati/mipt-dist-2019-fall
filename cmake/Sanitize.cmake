# Address Sanitizer
# https://clang.llvm.org/docs/AddressSanitizer.html

set(ASAN_COMPILE_FLAGS -g -fsanitize=address,undefined -fno-sanitize-recover=all)
set(ASAN_LINK_FLAGS -fsanitize=address,undefined)

# Thread Sanitizer
# https://clang.llvm.org/docs/ThreadSanitizer.html

set(TSAN_COMPILE_FLAGS -g -fsanitize=thread -fno-sanitize-recover=all)
set(TSAN_LINK_FLAGS -fsanitize=thread)

if(ASAN)
    log_info("Sanitize with address sanitizer")
    add_compile_options(${ASAN_COMPILE_FLAGS})
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${ASAN_LINK_FLAGS}")
endif()

if(TSAN)
    log_info(STATUS "Sanitize with thread sanitizer")
    add_compile_options(${TSAN_COMPILE_FLAGS})
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${TSAN_LINK_FLAGS}")
endif()


## Build Types
#set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE}
#        CACHE STRING "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel tsan asan lsan msan ubsan"
#        FORCE)
#
## ThreadSanitizer
#set(CMAKE_C_FLAGS_TSAN
#        "-fsanitize=thread -g -O1"
#        CACHE STRING "Flags used by the C compiler during ThreadSanitizer builds."
#        FORCE)
#set(CMAKE_CXX_FLAGS_TSAN
#        "-fsanitize=thread -g -O1"
#        CACHE STRING "Flags used by the C++ compiler during ThreadSanitizer builds."
#        FORCE)
#
## AddressSanitize
#set(CMAKE_C_FLAGS_ASAN
#        "-fsanitize=address -fno-optimize-sibling-calls -fsanitize-address-use-after-scope -fno-omit-frame-pointer -g -O1"
#        CACHE STRING "Flags used by the C compiler during AddressSanitizer builds."
#        FORCE)
#set(CMAKE_CXX_FLAGS_ASAN
#        "-fsanitize=address -fno-optimize-sibling-calls -fsanitize-address-use-after-scope -fno-omit-frame-pointer -g -O1"
#        CACHE STRING "Flags used by the C++ compiler during AddressSanitizer builds."
#        FORCE)
#
## LeakSanitizer
#set(CMAKE_C_FLAGS_LSAN
#        "-fsanitize=leak -fno-omit-frame-pointer -g -O1"
#        CACHE STRING "Flags used by the C compiler during LeakSanitizer builds."
#        FORCE)
#set(CMAKE_CXX_FLAGS_LSAN
#        "-fsanitize=leak -fno-omit-frame-pointer -g -O1"
#        CACHE STRING "Flags used by the C++ compiler during LeakSanitizer builds."
#        FORCE)
#
## MemorySanitizer
#set(CMAKE_C_FLAGS_MSAN
#        "-fsanitize=memory -fno-optimize-sibling-calls -fsanitize-memory-track-origins=2 -fno-omit-frame-pointer -g -O2"
#        CACHE STRING "Flags used by the C compiler during MemorySanitizer builds."
#        FORCE)
#set(CMAKE_CXX_FLAGS_MSAN
#        "-fsanitize=memory -fno-optimize-sibling-calls -fsanitize-memory-track-origins=2 -fno-omit-frame-pointer -g -O2"
#        CACHE STRING "Flags used by the C++ compiler during MemorySanitizer builds."
#        FORCE)
#
## UndefinedBehaviour
#set(CMAKE_C_FLAGS_UBSAN
#        "-fsanitize=undefined"
#        CACHE STRING "Flags used by the C compiler during UndefinedBehaviourSanitizer builds."
#        FORCE)
#set(CMAKE_CXX_FLAGS_UBSAN
#        "-fsanitize=undefined"
#        CACHE STRING "Flags used by the C++ compiler during UndefinedBehaviourSanitizer builds."
#        FORCE)