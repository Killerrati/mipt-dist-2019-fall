include(FindPythonInterp)
if (NOT PYTHONINTERP_FOUND)
    message(FATAL_ERROR "Failed locating python interpreter. Please install it. ${PYTHONINTERP_FOUND}")
endif()

execute_process(COMMAND ${PYTHON_EXECUTABLE} -c "import pytest; print(pytest.__version__)"
        RESULT_VARIABLE pytest_not_found OUTPUT_VARIABLE pytest_version ERROR_QUIET)
if(pytest_not_found)
    message(FATAL_ERROR "Running the tests requires pytest. Please install it manually"
            " (try: ${PYTHON_EXECUTABLE} -m pip install pytest)")
elseif(pytest_version VERSION_LESS 3.0)
    message(FATAL_ERROR "Running the tests requires pytest >= 3.0. Found: ${pytest_version}"
            "Please update it (try: ${PYTHON_EXECUTABLE} -m pip install -U pytest)")
endif()

set(OUTPUT_LOGS ${CMAKE_CURRENT_BINARY_DIR}/output)
file(MAKE_DIRECTORY ${OUTPUT_LOGS})

# log_warning("See tests outputs at ${OUTPUT_LOGS}")

add_custom_target(
        test_hello_world
        COMMAND
            ${CMAKE_COMMAND} -E env PYTHONNOUSERSITE=1 PYTHONPATH="${COURSE_ROOT}/common/python"
            ${PYTHON_EXECUTABLE} -B -m pytest -v  -s -r a -p no:cacheprovider --log-cli-level warning
            --log-file=${OUTPUT_LOGS}/test.log --log-file-level debug --basetemp="${OUTPUT_LOGS}/pytest"
            ${CMAKE_CURRENT_SOURCE_DIR}
            --solution "$<TARGET_FILE:hello_world>"
            --course-root "${COURSE_ROOT}"
            --build-root "${CMAKE_BINARY_DIR}"
            --logs-root "${OUTPUT_LOGS}"
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        USES_TERMINAL
)
add_dependencies(test_hello_world hello_world)