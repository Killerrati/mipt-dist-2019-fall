set(SRCS hello_world.cpp)
add_executable(hello_world ${SRCS})
include_directories(${COURSE_ROOT}/common)
include_directories(${SOLUTIONS_ROOT}/0-hello-world)
target_link_libraries(hello_world zookeeper)
target_link_libraries(hello_world arcagg)

add_subdirectory(tests)