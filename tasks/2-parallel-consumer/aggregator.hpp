#pragma once

#include <random>

namespace NSolution {

size_t RandomUInteger(size_t max) {
  static thread_local std::mt19937 generator(std::random_device{}());

  std::uniform_int_distribution<size_t> distribution(0, max);
  return distribution(generator);
}

class TAggregator {
  const static int modulo_ = 1581949;
 public:
  explicit TAggregator(int initial_value) : initial_value_(initial_value) {
    // noop
  }

  void Add(int x) {
    initial_value_ = (initial_value_ + x) % modulo_;
    std::this_thread::sleep_for(std::chrono::milliseconds(RandomUInteger(100)));
  }

  void Sub(int x) {
    initial_value_ = initial_value_ - x;
    std::this_thread::sleep_for(std::chrono::milliseconds(RandomUInteger(100)));
  }

  int Get() {
    return initial_value_;
  }

 private:
  int initial_value_;
};
} // namespace NSolution