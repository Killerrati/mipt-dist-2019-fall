#include <argagg/argagg.hpp>
#include <iostream>
#include <cstdlib>
#include <parallel_consumer.hpp>

int main(int argc, char **argv) {
  argagg::parser argument_parser{
      {
          {
              "cluster", {"-c", "--cluster"},
              "Zookeeper cluster connection string", 1
          },
          {
              "root", {"-r", "--root"},
              "Zookeeper root", 1
          },

      }
  };

  argagg::parser_results args;
  try {
    args = argument_parser.parse(argc, argv);
  } catch (const std::exception &e) {
    argagg::fmt_ostream fmt(std::cerr);
    fmt << "Bad usage!" << argument_parser << std::endl
        << "Encountered exception while parsing arguments: " << e.what()
        << std::endl;
    return EXIT_FAILURE;
  }

  if (!args["cluster"] || !args["root"]) {
    std::cout << "Bad usage! --cluster and --root required." << std::endl;
    return EXIT_FAILURE;
  }

  std::string cluster = args["cluster"];
  std::string root = args["root"];

  NSolution::TSolution solution{cluster, root};
  solution.Run();

  return EXIT_SUCCESS;
}
