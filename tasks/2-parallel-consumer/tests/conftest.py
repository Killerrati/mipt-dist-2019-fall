# noinspection PyUnresolvedReferences
from dsys.zookeeper_distribution import zookeeper_dist
# noinspection PyUnresolvedReferences
from dsys.docker_fixtures import docker_network, docker_client


def pytest_addoption(parser):
    parser.addoption('--zookeeper', help='path to ZooKeeper directory')
    parser.addoption('--solution', help='path to user solution')
    parser.addoption('--course-root', help='Path to course repository')
    parser.addoption('--build-root', help='Build root')
    parser.addoption('--logs-root', help='Tests output')
