#pragma once

namespace NSolution {
class TAggregator {
  const static int modulo_ = 1581949;
 public:
  explicit TAggregator(int initial_value) : initial_value_(initial_value) {
    // noop
  }

  void Add(int x) {
    initial_value_ = (initial_value_ + x) % modulo_;
  }

  void Div(int x) {
    initial_value_ = initial_value_ / x;
  }

  int Get() {
    return initial_value_;
  }

 private:
  int initial_value_;
};
} // namespace NSolution