set(SRCS sequential_consumer.cpp)
add_executable(sequential_consumer ${SRCS})
include_directories(${COURSE_ROOT}/common)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${SOLUTIONS_ROOT}/1-sequential-consumer)
target_link_libraries(sequential_consumer zookeeper)
target_link_libraries(sequential_consumer arcagg)

add_subdirectory(tests)